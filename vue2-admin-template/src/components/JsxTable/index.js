export default {
    name: "JsxTable",
    props: {
        // 是否可以选择
        hasSelection: {
            type: Boolean,
            default: () => false
        },
        // 是否展示序号列
        hasIndex: {
            type: Boolean,
            default: () => false
        },
        // 表头
        columns: {
            type: Array,
            default: () => []
        },
        // 数据
        dataList: {
            type: Array,
            default: () => []
        },
        // 加载状态
        loading: {
            type: Boolean,
            default: () => false
        }
    },
    data() {
        return {
        };
    },
    methods: {

    },
    render() {
        const { loading, hasSelection, hasIndex, columns, dataList } = this.$props;

        // 渲染序号
        const columnNumber = hasIndex ? (
            <el-table-column
                type="index"
                width="120"
                label="序号"
            />
        ) : null;

        // 渲染复选框
        const columnSelect = hasSelection ? (
            <el-table-column
                type="selection"
            />) : null;

        // 渲染列
        const renderColumn = () => {
            return columns.map(item => {
                const attribute = {
                    // attrs是用于将父组件属性传递（除了 prop 传递的属性、class 和 style ）给子组件, 这通常用于将事件监听器和自定义属性传递给子组件
                    attrs: { ...item }
                };
                // 这里使用的就是作用域插槽是用于将父组件的作用域插槽（scoped slot）传递给子组件，
                // 以便子组件可以在父组件提供的数据上进行渲染。在父组件中，

                // 标签并设置slot-scope属性来创建作用域插槽，
                // 然后在子组件中使用this.$slots属性来获取这些插槽
                if (item.slot) {
                    // scopedSlots
                    attribute.scopedSlots = {
                        default: this.$scopedSlots[item.slot]
                    };
                }
                return <el-table-column {...attribute} />;
            });
        };

        // @selection-change语法糖没法在jsx直接使用，需要转译一下
        const listeners = {
            on: {
                ['selection-change']: val => this.$emit('commitSelection', val)
            }
        };

        // v-loading没法在jsx直接使用，需要转译一下
        const directives = {
            directives: [{ name: 'loading', value: loading }]
        };

        // 渲染表格
        const renderTable = (
            <el-table data={dataList} {...listeners} {...directives} style="width: 100%" >
                {columnSelect}
                {columnNumber}
                {renderColumn()}
            </el-table >
        );
        return <div>{renderTable}</div>;
    }
};
