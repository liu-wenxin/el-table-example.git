import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tagsList: [],
  },
  mutations: {
    setTagsList(state, data) {
      state.tagsList = data;
    },
    /*  delTagsItem(state, data) {
       state
         .tagsList
         .splice(data.index, 1);
     },
     setTagsItem(state, data) {
       state
         .tagsList
         .push(data)
     },
     clearTags(state) {
       state.tagsList = []
     },
     closeTagsOther(state, data) {
       state.tagsList = data;
     }, */
  },
  actions: {
  },
  modules: {
  }
})
