import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "./plugins/index"
import "./assets/css/main.css";

/* 全局注册的组件 */
import './components/Table/index'; // 表格组件

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
